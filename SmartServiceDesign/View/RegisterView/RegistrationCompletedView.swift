//
//  RegistrationCompletedView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/12/23.
//

import SwiftUI

struct RegistrationCompletedView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    init() {
        let appearance = UINavigationBarAppearance()
        //        appearance.configureWithOpaqueBackground()
        //        appearance.backgroundColor = UIColor(named: "textColorHint") // 원하는 색상으로 변경하세요.
        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
    }
    
    @State var loginView: Bool = false
    
    var body: some View {
        
        
        
        NavigationView{
            ZStack {
                if loginView{
                    
                    
                        LoginView()
                    
                }else{
                    VStack{
                        Spacer()
                        
                        Image("imgComplete")
                        
                        
                        Text("가입이 완료되었습니다.")
                            .font(.title2)
                            .padding(.top, 40)
                        
                        Text("승인 후 이용 가능합니다.\n관리자에게 승인을 요청해주세요.")
                            .font(.system(size: 14))
                            .multilineTextAlignment(.center)
                            .foregroundColor(Color("color979797"))
                            .padding(.top, 27)
                        
                        
                        
                        Spacer()
                        
                        Button(action: {
                            print("loginBtn Click")
                            
                            loginView = true
                            
                        }, label: {
                            
                            Text("확인")
                                .foregroundColor(.white)
                                .font(.system(size: 18))
                                .bold()
                                .frame(width: screenWidth/1.14, height: screenHeight/15.03)
                            
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/41.066)
                        .padding(.bottom, 29)
                        
                        
                    }//VStack
                    .navigationBarBackButtonHidden(true)
                    .navigationBarItems(leading: Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        
                        HStack(spacing: 0){
                            
                            Image(systemName: "chevron.backward")
                            
                        }
                        
                        
                        
                    })
                    .navigationBarTitle("가입완료")
                }
                
                
            }//ZStack
           
            
        }//NavigationView
        //    .toast(isShowing: $showToast, text: Text(toastText))
        .navigationBarHidden(true)
        .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
            UIApplication.shared.endEditing()
            
        }
    
        
    }
    
}

#Preview {
    RegistrationCompletedView()
}
