//
//  agreementToGuideView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/12/23.
//

import SwiftUI
//import UIKit
import WebKit

struct agreementToGuideView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State var allGuideAgreeChecked: Bool = false
    @State var guideAgreeChecked: Bool = false
    @State var personalInformationChecked: Bool = false
    
    let guidAgreeUrl: String = "https://www.naver.com/"
    let personalInformationUrl: String = "https://www.google.com/"
    
//    @State private var showToast: Bool = false
//    @State private var toastText: String = ""
    
    init() {
        let appearance = UINavigationBarAppearance()
        //        appearance.configureWithOpaqueBackground()
        //        appearance.backgroundColor = UIColor(named: "textColorHint") // 원하는 색상으로 변경하세요.
        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
    }
    
    var body: some View {
        
        NavigationView{
            
            ZStack{
                
                VStack(spacing: 0){
                    
                    
                    Button(action: {
                        
                        allGuideAgreeChecked.toggle()
                        
                        if allGuideAgreeChecked{
                            guideAgreeChecked = true
                            personalInformationChecked = true
                        }
                        
                        
                    }, label: {
                        
                        HStack{
                            
                            Image(systemName: allGuideAgreeChecked ? "checkmark.square.fill" : "square")
                                .foregroundColor(.blue)
                                .cornerRadius(4)
                                .padding(.leading, 15)
                            
                            Text("약관 전체 동의")
                                .foregroundColor(.black)
                                .font(.system(size: 16))
                            
                            Spacer()
                        }
                        
                        
                    })
                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                    .background(Color("colorDEE3FF"))
                    .cornerRadius(4)
                    .overlay(RoundedRectangle(cornerRadius: 4)
                        .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                    )
                    .padding(.top, screenHeight/15.61)
                    .padding(.bottom, 10)
                    
                    
                    
                    
                    
                    Rectangle()
                        .frame(width: screenWidth/1.14, height: screenHeight/8.28)
                        .cornerRadius(4)
                        .foregroundColor(.white)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        .overlay(content: {
                            
                            VStack(spacing: 0){
                                HStack{
                                    
                                    Button(action: {
                                        guideAgreeChecked.toggle()
                                        
                                        
                                    }, label: {
                                        
                                        HStack{
                                            
                                            Image(systemName: guideAgreeChecked ? "checkmark.square.fill" : "square")
                                                .foregroundColor(.blue)
                                                .cornerRadius(4)
                                                .padding(.leading, 15)
                                            
                                            Text("[필수] 이용약관")
                                                .foregroundColor(.black)
                                                .font(.system(size: 14))
                                            Spacer()
                                        }
                                    })
                                    
                                    Button(action: {
                                        print("이용약관 화살표 클릭")
                                    }, label: {
                                        NavigationLink{
                                            WKWebViewPractice(url: guidAgreeUrl)
                                        } label: {
                                            Image(systemName: "chevron.right")
                                                .padding(15)
                                                .foregroundColor(Color("colorE0E0E0"))
                                        }
                                       
                                    })
                                    
                                }
                                
                                HStack{
                                    Button(action: {
                                        personalInformationChecked.toggle()
                                    }, label: {
                                        Image(systemName: personalInformationChecked ? "checkmark.square.fill" : "square")
                                            .foregroundColor(.blue)
                                            .cornerRadius(4)
                                            .padding(.leading, 15)
                                        
                                        Text("[필수] 개인정보 수집 및 이용 동의")
                                            .foregroundColor(.black)
                                            .font(.system(size: 14))
                                        
                                        
                                        Spacer()
                                    })
                                    
                                    Button(action: {
                                        print("개인정보 수집 및 이용 동의 화살표 클릭")
                                        
                                        
                                    }, label: {
                                        NavigationLink{
                                            WKWebViewPractice(url: personalInformationUrl)
                                        } label: {
                                            Image(systemName: "chevron.right")
                                                .padding(15)
                                                .foregroundColor(Color("colorE0E0E0"))
                                        }
                                        
                                    })
                                    
                                    
                                }
                                
                                
                                
                                
                            }//VStack
                            
                        })//Rectangle 이용약관과 개인정보 수집 및 이용 동의 박스
                    
                    
                    Spacer()
                    
                    ZStack{
                        
                        
                        
                        Button(action: {
                            print("UserInfoView 로 이동하는 다음 버튼 클릭")
                            
                        }, label: {
                            
                            NavigationLink{
                                UserInfoView()
                            } label: {
                                
                                Text("다음")
                                    .foregroundColor(.white)
                                    .font(.system(size: 18))
                                    .frame(width: screenWidth/1.14, height: screenHeight/15.03)
                                    .bold()
                            }
                             
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/41.066)
                        .padding(.bottom, 29)
                        
                        if !guideAgreeChecked || !personalInformationChecked {
                            Button(action: {
                                
                                print("필수 항목 동의 안했을 때 클릭되는 다음 버튼")
                                
//                                    toastText = "필수 항목에 동의해주세요"
//                                    showToast = true
//
//                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//                                        self.showToast = false
//                                        toastText = ""
//                                    }
                                
                            }) {
                                Text("다음")
                                    .foregroundColor(.white)
                                    .font(.system(size: 18))
                                    .frame(width: screenWidth/1.14, height: screenHeight/15.03)
                                    .bold()
                            }
                            .background(Color.gray)
                            .cornerRadius(4)
                            .padding(.top, screenHeight/41.066)
                            .padding(.bottom, 29)
                            
                            
                        }
                    }
                    
                    
                    
                    
                    
                }//VStack
   
                
                
                
            }//ZStack
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                
                HStack(spacing: 0){
                    
                    Image(systemName: "chevron.backward")
                    
                }
                
                
                
            })
            .navigationBarTitle("약관동의")
            
            
            
            
            
        }//NavigationView
//        .toast(isShowing: $showToast, text: Text(toastText))
        .navigationBarHidden(true)
        
        
        
        
        
    }
}

#Preview {
    agreementToGuideView()
}
