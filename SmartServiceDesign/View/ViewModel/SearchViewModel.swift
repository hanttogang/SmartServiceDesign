//
//  SearchViewModel.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/11/24.
//

import Foundation

class SearchViewModel: ObservableObject {
    
    
    @Published var navigateECUserSearchView: Bool = false
    @Published var userType: String = "employee"
    
    
    @Published var userIdx: Int = 0
    @Published var userName: String = ""
    @Published var userPhoneNum: String = ""
    @Published var userEmail: String = ""
    
    @Published var navigateAllUserSearchView: Bool = false
    
    @Published var humanResourceUsersIdx: Int = 0
    @Published var humanResourceUsersName: String = ""
}
