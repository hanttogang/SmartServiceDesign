//
//  ThirdCustomerRegistrationView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/20/23.
//

import SwiftUI
import UIKit
import Alamofire

struct ThirdCustomerRegistrationView: View {
    @EnvironmentObject var registrationCustomerBrandData: RegistrationCustomerBrandData
    @EnvironmentObject var loginData: LoginData
    
    let defaultUrl = "\(ApiClient.BASE_URL)"
    @State private var finalKeyValue: String = ""
    
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showingImagePickerForBrandImage = false
    @State private var showingImagePickerForBusinessLicenseImage = false
    @State private var showingImagePickerForBankRecordCopyImage = false
    
    @State private var selectedBrandImage: UIImage? = nil
    @State private var selectedBusinessLicenseImage: UIImage? = nil
    @State private var selectedBankRecordCopyImage: UIImage? = nil
    
    @State private var navigate: Bool = false
    
    @State private var isApiLoading: Bool = false
    
    
    var body: some View {
        
        ZStack {
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView{
                VStack(spacing: 0){
                    
                    ZStack{
                        
                       
                        
                        HStack{
                            
                            
                            ZStack{
                                Button(action: {
                                    self.presentationMode.wrappedValue.dismiss()
                                }, label: {
                                    Text("이전")
                                        .foregroundColor(.black)
                                    
                                })
                                .frame(width: screenWidth/7.211, height: screenHeight/31.346)
                                .background(Color("colorE5E5E5"))
                                .cornerRadius(4.0)
                            }
                            
                            Spacer()
                        }
                        .padding()
                        
                        HStack{
                            Spacer()
                            
                            HStack{
                                Circle()
                                    .frame(width: screenWidth/46.875)
                                    .foregroundColor(Color("colorE0E0E0"))
                                
                                Circle()
                                    .frame(width: screenWidth/46.875)
                                    .foregroundColor(Color("colorE0E0E0"))
                                
                                Circle()
                                    .frame(width: screenWidth/46.875)
                                    .foregroundColor(Color("mainColor"))
                                
                                Circle()
                                    .frame(width: screenWidth/46.875)
                                    .foregroundColor(Color("colorE0E0E0"))
                                
                            }
                            Spacer()
                            
                        }
                        
                        HStack{
                            Spacer()
                            
                            ZStack{
                                Button(action: {
                                    
                                    if (!isApiLoading){
                                        
                                        if(selectedBrandImage == nil || selectedBusinessLicenseImage == nil || selectedBankRecordCopyImage == nil){
                                            
                                            showAlert = true
                                            
                                            alertTitle = "마케팅 채널 등록 실패"
                                            alertMessage = "이미지를 비울 수 없습니다."
                                            
                                        }else{
                                            
                                            
                                            registrationCustomerBrand()
                                            
                                            //                                            self.navigate = true
                                            
                                        }
                                        
                                    }
                                    
                                    
                                    
                                    
                                    
                                }, label: {
                                    
                                    
                                    NavigationLink(destination: CompleteCustomerRegistrationView(), isActive: $navigate) {
                                        EmptyView()
                                    }
                                    
                                    Text("다음")
                                        .foregroundColor(.white)
                                    
                                })
                                .frame(width: screenWidth/7.211, height: screenHeight/31.346)
                                .background(.blue)
                                .cornerRadius(4.0)
                            }
                        }
                        .padding()
                        
                        
                    }
                    .padding(.top, screenHeight/45.1) // 18
                    
                    
                    
                    VStack(alignment: .leading){// 브랜드 이미지
                        
                        HStack(spacing: 0){
                            Text("브랜드 이미지")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        Button(action: {
                            showingImagePickerForBrandImage = true
                        }, label: {
                            
                            ZStack {
                                if let image = selectedBrandImage {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                    }//VStack 브랜드 이미지
                    .padding(.top, screenHeight / 30.185) //20
                    .sheet(isPresented: $showingImagePickerForBrandImage, onDismiss: loadBrandImage) {
                        ImagePicker(selectedImage: $selectedBrandImage)
                    }
                    
                    
                    
                    VStack(alignment: .leading){// 사업자등록증
                        HStack(spacing: 0){
                            Text("사업자등록증")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        Button(action: {
                            showingImagePickerForBusinessLicenseImage = true
                        }, label: {
                            
                            ZStack {
                                if let image = selectedBusinessLicenseImage {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                            
                            
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                    }//VStack 사업자 등록증
                    .padding(.top, screenHeight / 30.185) //20
                    .sheet(isPresented: $showingImagePickerForBusinessLicenseImage, onDismiss: loadBusinessLicenseImage) {
                        ImagePicker(selectedImage: $selectedBusinessLicenseImage)
                    }
                    
                    
                    
                    
                    
                    VStack(alignment: .leading){// 통장사본
                        HStack(spacing: 0){
                            Text("통장사본")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        Button(action: {
                            showingImagePickerForBankRecordCopyImage = true
                        }, label: {
                            
                            ZStack {
                                if let image = selectedBankRecordCopyImage {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                        
                    }//VStack 통장사본
                    .padding(.top, screenHeight / 30.185) //20
                    .sheet(isPresented: $showingImagePickerForBankRecordCopyImage, onDismiss: loadBankRecordCopyImage) {
                        ImagePicker(selectedImage: $selectedBankRecordCopyImage)
                    }
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/8.826)
                    
                    Spacer()
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("고객(브랜드) 등록", displayMode: .inline)
                
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
            
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        
        
        
        
        
        
        
        
        
    }//body
    
    func loadBrandImage() {
        guard let selectedBrandImage = selectedBrandImage else { return }
        
        
        isApiLoading = true
        
        // You can do something with the selected brand image here
        uploadImage(image: selectedBrandImage, imageType: "brand")
        print(registrationCustomerBrandData.brandImage)
    }
    
    func loadBusinessLicenseImage() {
        guard let selectedBusinessLicenseImage = selectedBusinessLicenseImage else { return }
        // You can do something with the selected business license image here
        
        isApiLoading = true
        
        uploadImage(image: selectedBusinessLicenseImage, imageType: "registration")
        print(registrationCustomerBrandData.registrationImage)
        
    }
    
    func loadBankRecordCopyImage() {
        guard let selectedBankRecordCopyImage = selectedBankRecordCopyImage else { return }
        // You can do something with the selected bank record copy image here
        
        isApiLoading = true
        
        uploadImage(image: selectedBankRecordCopyImage, imageType: "bankbook")
        print(registrationCustomerBrandData.bankbookCopy)
    }
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonBrandImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "mimetype": "image/png",
            "type": "brand",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        
        getApiCommonBrandImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                    print("Could not convert image to data.")
                    return
                }
                
                AF.upload(imageData, to: url!, method: .put).response { response in
                    switch response.result {
                    case .success:
                        print("Image uploaded successfully.")
                        
                        isApiLoading = false
                        
                    case .failure(let error):
                        print("Failed to upload image: \(error)")
                    }
                }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "brand":
                        registrationCustomerBrandData.brandImage = key!
                        
                        print("\(registrationCustomerBrandData.brandImage)")
                        
                    case "registration":
                        registrationCustomerBrandData.registrationImage = key!
                        
                        print("\(registrationCustomerBrandData.registrationImage)")
                        
                    case "bankbook":
                        registrationCustomerBrandData.bankbookCopy = key!
                        
                        print("\(registrationCustomerBrandData.bankbookCopy)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
        
        
    }
    
    
    //최종적으로 등록
    private func registrationCustomerBrand() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            "brand_name": registrationCustomerBrandData.brandName,
            "registration_number": registrationCustomerBrandData.registrationNumber,
            "buisness_type": registrationCustomerBrandData.businessType,
            "address": registrationCustomerBrandData.address,
            "address_detail": registrationCustomerBrandData.addressDetail,
            "representative_name": registrationCustomerBrandData.representativeName,
            "representative_phone": registrationCustomerBrandData.representativePhone,
            "representative_email": registrationCustomerBrandData.representativeEmail,
            "manager_name": registrationCustomerBrandData.managerName,
            "manager_phone": registrationCustomerBrandData.managerPhone,
            "manager_email": registrationCustomerBrandData.managerEmail,
            "manage_type": registrationCustomerBrandData.manageType,
            "brand_image": registrationCustomerBrandData.brandImage,
            "registration_image": registrationCustomerBrandData.registrationImage,
            "bankbook_copy": registrationCustomerBrandData.bankbookCopy
        ]
        
        
        AF.request("\(defaultUrl)/api/brand",
                   method: .post,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("고객 브랜드 등록 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("고객 브랜드 등록 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    self.navigate = true
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("고객 브랜드 등록 실패: \(error)")
                
            }
        }
    }
    
//    func convertImageToPNGAndSave(image: UIImage?) -> URL? {
//        guard let data = image?.pngData() else {
//            return nil
//        }
//        
//        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//        let fileName = UUID().uuidString + ".png" // 고유한 파일 이름을 생성합니다.
//        let fileURL = documentsDirectory.appendingPathComponent(fileName)
//        
//        do {
//            try data.write(to: fileURL)
//            print("파일이 성공적으로 저장되었습니다: \(fileName)")
//            return fileURL
//        } catch {
//            print("파일 저장 실패:", error)
//            return nil
//        }
//    }
//    
}

#Preview {
    ThirdCustomerRegistrationView()
}
