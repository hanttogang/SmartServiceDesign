//
//  CustomerSearchView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/14/23.
//

import SwiftUI

struct AddPersonInChargeView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State var searchManagerName: String = ""
    @State private var editText: Bool = false
    
    @State private var selectedManagerName: String = ""
    
    
    var body: some View {
        
        ZStack{
            VStack(spacing: 0){
                
                
                HStack{
                    
                    HStack{
                        
                    }
                    .frame(width: screenWidth, height: screenHeight/14.5)
                    .overlay{
                        
                        HStack{
                            
                            //검색창을 받을수있는 택스트필드
                            TextField("Search" , text : self.$searchManagerName)
                                .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                .foregroundColor(Color(.black))
                                .padding(10)
                                .padding(.leading, screenWidth/12.5) //30
                                .background(Color(.white))
                                .frame(width: screenWidth/1.3)
                                .cornerRadius(15)
                                .overlay(
                                    
                                    //가로로 view를 쌓을수있도록 하나 만들고
                                    HStack{
                                        Image(systemName: "magnifyingglass")
                                            .foregroundColor(Color("hintTextColor"))
                                            .padding()
                                        
                                        Spacer()
                                        
                                        if self.editText{
                                            Button(action : {
                                                
                                                self.editText = false
                                                self.searchManagerName = ""
                                                
                                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                            }){
                                                Image(systemName: "multiply.circle.fill")
                                                    .foregroundColor(Color("hintTextColor"))
                                                    .padding()
                                            }
                                        }
                                        
                                        
                                    }//HStack
                                    
                                    
                                    
                                    
                                    
                                    
                                )//overlay
                                .onTapGesture {
                                    self.editText = true
                                }
                            
                            Button(action: {
                                
                                print("취소 버튼 클릭")
                                
                                
                            }, label: {
                                Text("취소")
                            })
                            .padding(.horizontal, screenWidth/46.875)
                            //                            }
                            
                            
                        }
                    }//overlay
                }//HStack 검색창
                .background(Color("colorF8F8F892"))
                
                HStack{
                    Text("담당자 검색")
                        .font(.title2)
                        .padding(.leading, screenWidth/23.4375) //16
                        .padding(.top, screenWidth/20.833) //18
                    
                    Spacer()
                }
                Divider() // 가로 선
                    .padding(.top, 8)
                    .padding(.leading, screenWidth/23.4375) //16
                
                List(dummyData.filter{ searchManagerName.isEmpty ? true : $0.name.contains(searchManagerName)}) { company in
                    VStack(alignment: .leading){
                        HStack(spacing: 0){
                            
                            Text(company.name)
                                .frame(minWidth: 0, minHeight: 0, maxHeight: .infinity)
                                .foregroundColor(.blue)
                            
                            
                            Spacer()
                        }
                        
                        
                    }
                    .swipeActions {
                        Button(action: {
                            print("제외 버튼 클릭")
                            // 선택 버튼 클릭 시 수행할 작업을 여기에 추가합니다.
                        }) {
                            Text("제외")
                        }
                        .tint(.red)
                        
                        Button(action: {
                            print("선택 버튼 클릭")
                            // 제외 버튼 클릭 시 수행할 작업을 여기에 추가합니다.
                        }) {
                            Text("선택")
                        }
                        .tint(.gray)
                        
                        
                    }
                    .onTapGesture {
                        print(company.name)
                        
                        selectedManagerName = "\(company.name)"
                        
                        
                    }
                    
                    
                }//List
                .listStyle(PlainListStyle())
                .background(.white)
                
                
                Spacer()
                
            } //VStack
            .navigationBarHidden(true)
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
        }//ZStack
        
    }
}

#Preview {
    AddPersonInChargeView()
}
