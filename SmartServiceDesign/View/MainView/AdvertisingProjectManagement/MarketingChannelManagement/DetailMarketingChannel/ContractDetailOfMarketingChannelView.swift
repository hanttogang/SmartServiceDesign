//
//  ContractDetailOfMarketingChannelView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI
import Alamofire

struct ContractDetailOfMarketingChannelView: View {
    
    //Api
    var selectedMarketingChannelContractIdx: Int
    
    var selectedMarketingIdx: Int //임시 저장
    var detailMarketingChannelView: DetailMarketingChannelView
    
    // 이 init 메소드를 통해 외부에서 selectedMarketingChannelContractIdx 값을 설정할 수 있습니다.
    init(selectedMarketingChannelContractIdx: Int, selectedMarketingIdx: Int) {
        self.selectedMarketingChannelContractIdx = selectedMarketingChannelContractIdx
        
        self.selectedMarketingIdx = selectedMarketingIdx
        self.detailMarketingChannelView = DetailMarketingChannelView(selectedMarketingIdx: selectedMarketingIdx)
    }
    
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var channelContractImageStringFor_DetailContractBrand = ""
    @State private var channelContractDataList = [ChannelContractData]()
    
    @State private var isApiLoading: Bool = false
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var navigate: Bool = false
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var showToast: Bool = false
    @State private var toastText: String = "시작 날짜 선택 후 확인을 눌러주세요"
    
    @State private var modificationModeFor_DetailContractMarketingChannel: Bool = false
    
    
    @State private var contractNameFor_DetailContractMarketingChannel: String = ""
    @State private var contractOneLineExplanFor_DetailContractMarketingChannel: String = ""
    @State private var contractStartDateFor_DetailContractMarketingChannel: String = ""
    @State private var contractEndDateFor_DetailContractMarketingChannel: String = ""
    @State private var contractPriceFor_DetailContractMarketingChannel: String = ""
    @State private var contractStatusFor_DetailContractMarketingChannel: String = ""
    
    
    @State private var selecteContractStatusFor_DetailContractMarketingChannel: Bool = false
    @State private var contractStatusModificationModeFor_DetailContractMarketingChannel: Bool = false
    
    @State private var showingBusinessLicenseImagePickerFor_DetailContractMarketingChannel = false
    @State private var selectedBusniessLicenseImageFor_DetailContractMarketingChannel: UIImage? = nil
    
    //날짜 관련
    @State private var showingDatePicker = false
    @State private var dateSelection = Date()
    @State private var startDate: Date? = nil
    @State private var endDate: Date? = nil
    @State private var dateCount = 0
    
  
    
    var body: some View {
        
        ZStack {
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    HStack{
                        
                    
                        
                        if modificationModeFor_DetailContractMarketingChannel{
                            
                            TextField(".", text: $contractNameFor_DetailContractMarketingChannel, prompt: Text("\(contractNameFor_DetailContractMarketingChannel)")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.title2)
                            .frame(height: screenHeight/29) //28
                            
                            
                        }else {
                            
                            Text("\(contractNameFor_DetailContractMarketingChannel)")
                                .font(.title2)
                                .frame(height: screenHeight/29) //28
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationModeFor_DetailContractMarketingChannel){
                            Button(action: {
                                
                                if (!isApiLoading){
                                    
                                    
                                    if (contractNameFor_DetailContractMarketingChannel == "" || contractOneLineExplanFor_DetailContractMarketingChannel == "" || contractStartDateFor_DetailContractMarketingChannel == "" || contractEndDateFor_DetailContractMarketingChannel == "" || contractPriceFor_DetailContractMarketingChannel == ""){
                                        
                                        showAlert = true
                                        
                                        alertTitle = "계약 수정 실패"
                                        alertMessage = "칸을 비울 수 없습니다."
                                        
                                    } else {
                                        patchCustomerBrandContract()
                                        modificationModeFor_DetailContractMarketingChannel = false
                                    }
                                    
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationModeFor_DetailContractMarketingChannel = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                            
                        }
                        
                        
                    }// HStack 계약 이름
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
//                    .padding(.top, screenHeight / 81.5)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        
                        Text("한줄 설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailContractMarketingChannel{
                            
                            TextField("", text: $contractOneLineExplanFor_DetailContractMarketingChannel)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                            
//                            RightAlignedTextField(text: $contractOneLineExplanFor_DetailContractMarketingChannel)
//                                .keyboardType(.numberPad)
                            
                            
                            
                        }else {
                            
                            Text("\(contractOneLineExplanFor_DetailContractMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("계약기간")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailContractMarketingChannel{
                            
                            Button {
                                
                                startDate = nil
                                endDate = nil
                                
                                self.showingDatePicker = true
                                
                                toastText = "시작 날짜 선택 후 확인을 눌러주세요"
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                                
                            } label: {
                                
                                
                                Text("\(contractStartDateFor_DetailContractMarketingChannel) ~ \(contractEndDateFor_DetailContractMarketingChannel)")
                                    .foregroundColor(Color("color00000040"))

                            }

                            
                            
                        }else {
                            
                            Text("\(contractStartDateFor_DetailContractMarketingChannel) ~ \(contractEndDateFor_DetailContractMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .sheet(isPresented: $showingDatePicker) {
                        VStack {
                            
                                DatePicker("날짜를 선택해주세요", selection: $dateSelection, displayedComponents: .date)
                                    .datePickerStyle(GraphicalDatePickerStyle())
                                    .padding()
                                    .overlay{
                                        ZStack{}
                                        .toast(isShowing: $showToast, text: Text(toastText))
                                    }
                            
                            
                            
                            Button("확인") {
                                if self.dateCount == 0 {
                                    self.startDate = dateSelection
                                    self.dateCount += 1
                                } else if self.dateCount == 1 {
                                    if dateSelection > self.startDate! {
                                        self.endDate = dateSelection
                                        self.dateCount = 0
                                    } else {
                                        self.startDate = nil
                                        self.endDate = nil
                                        self.dateCount = 0
                                    }
                                }
                                
                                
                                if (startDate != nil && endDate == nil){
                                    
                                    toastText = "종료 날짜 선택 후 확인을 주세요"
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                    self.showingDatePicker = true
                                }
                                
                                
                                if (startDate != nil && endDate != nil){
                                    contractStartDateFor_DetailContractMarketingChannel = convertDateToString(date: startDate)
                                    contractEndDateFor_DetailContractMarketingChannel = convertDateToString(date: endDate)
                                    
                                    self.showingDatePicker = false
                                }else{
                                    contractStartDateFor_DetailContractMarketingChannel = ""
                                    contractEndDateFor_DetailContractMarketingChannel = ""
                                }
                            }
                        }
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("계약금액")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailContractMarketingChannel{
                            
                            TextField("", text: $contractPriceFor_DetailContractMarketingChannel)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                .keyboardType(.numberPad)
                            
//                            RightAlignedTextField(text: $contractPriceFor_DetailContractMarketingChannel)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                            
                            
                        }else {
                            
                            Text("\(contractPriceFor_DetailContractMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack(spacing: 0){
                        Text("진행상태")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        HStack(spacing: 0){
                            
                            if modificationModeFor_DetailContractMarketingChannel{
                                
                                
                                Button(action: {
                                    selecteContractStatusFor_DetailContractMarketingChannel = true
                                }, label: {
                                    
                                    if selecteContractStatusFor_DetailContractMarketingChannel{
                                        
                                        HStack(spacing: screenWidth/128.3){ // spacing 3
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractMarketingChannel = false
                                                
                                                contractStatusFor_DetailContractMarketingChannel = "요청"
                                                
                                            }, label: {
                                                Text("요청")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractMarketingChannel = false
                                                
                                                contractStatusFor_DetailContractMarketingChannel = "진행"
                                                
                                            }, label: {
                                                Text("진행")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractMarketingChannel = false
                                                
                                                contractStatusFor_DetailContractMarketingChannel = "피드백"
                                                
                                            }, label: {
                                                Text("피드백")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractMarketingChannel = false
                                                
                                                contractStatusFor_DetailContractMarketingChannel = "완료"
                                                
                                            }, label: {
                                                Text("완료")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractMarketingChannel = false
                                                
                                                contractStatusFor_DetailContractMarketingChannel = "보류"
                                                
                                            }, label: {
                                                Text("보류")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                        } //HStack
                                        
                                        
                                    }else{
                                        
                                        Button(action: {
                                            selecteContractStatusFor_DetailContractMarketingChannel = true
                                            
                                        }, label: {
                                            Text("\(contractStatusFor_DetailContractMarketingChannel)")
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                        })
                                    }
                                    
                                })
                            } else{
                                
                                Text("\(contractStatusFor_DetailContractMarketingChannel)")
                                    .foregroundColor(.white)
                                    .bold()
                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                    .background(Color("mainColor"))
                                    .cornerRadius(16.0)
                            }
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                   
                    
                    
                    
                    HStack(spacing: 0){
                        
                        
                        VStack(alignment: .leading){// 사업자등록증 이미지
                            
                            Text("계약서 이미지")
                                .padding(.top, screenHeight/30.75)
                            
                            ZStack{
                                
                                
                                if modificationModeFor_DetailContractMarketingChannel{
                                    
                                    Button(action: {
                                        showingBusinessLicenseImagePickerFor_DetailContractMarketingChannel = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedBusniessLicenseImageFor_DetailContractMarketingChannel {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                AsyncImage(url: URL(string: imageS3Url + "/" + channelContractImageStringFor_DetailContractBrand)) { image in
                                                    image.resizable()
                                                         .aspectRatio(contentMode: .fill)
                                                         .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                         .cornerRadius(12.0)
                                                } placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                }
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedBusniessLicenseImageFor_DetailContractMarketingChannel {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            AsyncImage(url: URL(string: imageS3Url + "/" + channelContractImageStringFor_DetailContractBrand)) { image in
                                                image.resizable()
                                                     .aspectRatio(contentMode: .fill)
                                                     .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                     .cornerRadius(12.0)
                                            } placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                            }
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack 사업자등록증이미지
                            .sheet(isPresented: $showingBusinessLicenseImagePickerFor_DetailContractMarketingChannel, onDismiss: loadBusniessLicenseImageFor_DetailContractMarketingChannel) {
                                ImagePicker(selectedImage: $selectedBusniessLicenseImageFor_DetailContractMarketingChannel)
                            }
                            
                            
                            
                        }//VStack 사업자등록증 이미지
                        
                        
                        
                        Spacer()
                        
                        
                      
                    }//HStack
                    .padding(.leading)
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/8.826)
                    
                    Spacer()
                    
                } //VStack
                
            }//ScrollView
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            detailMarketingChannelView.contractInquiry(accessToken: loginData.token)
            
        }) {
            HStack(spacing: 0){
                Image(systemName: "chevron.backward")
            }
        })
        .navigationBarTitle("\(contractNameFor_DetailContractMarketingChannel)", displayMode: .inline)
        .onAppear{
            contractInquiry()
        }
        
      
        
        
        
    }//body

    
  
    private func loadBusniessLicenseImageFor_DetailContractMarketingChannel() {
        guard let selectedBusniessLicenseImageFor_DetailContractMarketingChannel = selectedBusniessLicenseImageFor_DetailContractMarketingChannel else { return }
        // You can do something with the selected brand image here
        isApiLoading = true
        
        uploadImage(image: selectedBusniessLicenseImageFor_DetailContractMarketingChannel, imageType: "channelContract")
    }
    
    
    
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonBrandImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "mimetype": "image/png",
            "type": "marketing_contract",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonBrandImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                    
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "channelContract":
                        channelContractImageStringFor_DetailContractBrand = key!
                        
                        print("\(channelContractImageStringFor_DetailContractBrand)")
                   
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    
    
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormatter.string(from: date)
//            print(dateString) //ex "2024.01.05" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }

    }
    
    
    
    //계약 가져오는 api
    private func contractInquiry() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/marketing-contract?marketing_contract_idx=\(selectedMarketingChannelContractIdx)", method: .get, headers: headers).responseDecodable(of: ChannelContractDataResponse.self) { response in
            switch response.result {
    
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                
                for channelContractData in response.data {
                    
                    print("광고 번호: \(channelContractData.marketingContractIdx)")
                    
                    contractNameFor_DetailContractMarketingChannel = channelContractData.contractName
                    contractOneLineExplanFor_DetailContractMarketingChannel = channelContractData.oneLineExplan
                    contractStartDateFor_DetailContractMarketingChannel = dateFormatter(getDate: channelContractData.contractStartDt)
                    contractEndDateFor_DetailContractMarketingChannel = dateFormatter(getDate: channelContractData.contractEndDt)
                    contractStatusFor_DetailContractMarketingChannel = channelContractData.contractStatus
                    contractPriceFor_DetailContractMarketingChannel = String(channelContractData.contractPrice)
                    channelContractImageStringFor_DetailContractBrand = channelContractData.contractImage
                    
                    
                }
                
                
                DispatchQueue.main.async {
//                    contractDataList = response.data
//
//                    contractCount = response.pagination.total
                    
//                    print("\(selectedCustomerBrandContractIdx) 계약 리스트 호출 성공 \(response.data)")
//
                }
                
                
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
    
    //최종적으로 계약 수정
    private func patchCustomerBrandContract() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            "contract_name": contractNameFor_DetailContractMarketingChannel,
             "one_line_explan": contractOneLineExplanFor_DetailContractMarketingChannel,
             "contract_start_dt": contractStartDateFor_DetailContractMarketingChannel,
             "contract_end_dt": contractEndDateFor_DetailContractMarketingChannel,
             "contract_price": Int(contractPriceFor_DetailContractMarketingChannel),
             "contract_status": contractStatusFor_DetailContractMarketingChannel,
             "contract_image": channelContractImageStringFor_DetailContractBrand
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/marketing-contract/\(selectedMarketingChannelContractIdx)",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("마케팅 채널 계약 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("마케팅 채널 계약 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("마케팅 채널 계약 수정 실패: \(error)")
                
            }
        }
    }
    
    func convertDateToString(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
}
//
//#Preview {
//    ContractDetailOfMarketingChannelView()
//}
