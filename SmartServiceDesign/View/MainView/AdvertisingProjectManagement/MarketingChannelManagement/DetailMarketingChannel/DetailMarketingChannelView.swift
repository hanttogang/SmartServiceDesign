//
//  DetailMarketingChannelView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI
import Alamofire

struct DetailMarketingChannelView: View {
    
    //Api
    var selectedMarketingIdx: Int
    
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandIdx 값을 설정할 수 있습니다.
    init(selectedMarketingIdx: Int) {
        self.selectedMarketingIdx = selectedMarketingIdx
    }
    
//    @State private var marketingDataList = [MarketingData]()
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var channelImageFor_DetailMarketingChannel: String = ""
    
    @State private var channelContractDataList = [ChannelContractData]()
    @State private var channelContractCount: Int = 0
    
    @State private var isApiLoading: Bool = false
    //View
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var navigate: Bool = false
    @State private var navigateContractDetailForDetailCustomerBrandView: Bool = false
    
    //Alert 관련 변수 설정
    @State private var showAlert: Bool = false
    @State private var alertTitle: String = ""
    @State private var alertMessage: String = ""
    
    @State private var modificationMode: Bool = false
    
    @State private var showingImagePickerFor_DetailMarketingChannel = false
    @State private var selectedDetailMarketingChannelImage: UIImage? = nil
    
    @State private var marketingChannelNameFor_DetailMarketingChannel: String = ""
    
    @State private var oneLineExplanFor_DetailMarketingChannel: String = ""
    
    @State private var managerNameFor_DetailMarketingChannel: String = ""
    @State private var managerPhoneNumberFor_DetailMarketingChannel: String = ""
    @State private var managerEmailFor_DetailMarketingChannel: String = "bca321@mail.com"
    
    @State private var workAreaFor_DetailMarketingChannel: String = "광고마케팅"
    @State private var selecteWorkAreaFor_DetailMarketingChannel: Bool = false
    @State private var workAreaModificationModeFor_DetailMarketingChannel: Bool = false
    
    @State private var showingBankRecordCopyImagePickerFor_DetailMarketingChannel = false
    @State private var selectedBankRecordCopyImageFor_DetailMarketingChannel: UIImage? = nil
    
    @State private var contractImageForDetailBrandContractList: UIImage? = nil
    
    
    @State private var showAllContractList: Bool = false
    @State private var showShowAllButton: Bool = false
    
    
    
    var body: some View {
        
        ZStack {
            
            if (isApiLoading){
                
                ProgressView()
                    .zIndex(3)
                
            }
            
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        
                        VStack(alignment: .leading){// 브랜드 이미지
                            ZStack{
                                if modificationMode{
                                    
                                    Button(action: {
                                        showingImagePickerFor_DetailMarketingChannel = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedDetailMarketingChannelImage {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                AsyncImage(url: URL(string: imageS3Url + "/" + channelImageFor_DetailMarketingChannel)) { image in
                                                    image.resizable()
                                                        .aspectRatio(contentMode: .fill)
                                                        .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                        .cornerRadius(12.0)
                                                }placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                }
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedDetailMarketingChannelImage {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            AsyncImage(url: URL(string: imageS3Url + "/" + channelImageFor_DetailMarketingChannel)) { image in
                                                image.resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                            }placeholder: {
                                                ProgressView()
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                            }
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack BrandImage
                            .sheet(isPresented: $showingImagePickerFor_DetailMarketingChannel, onDismiss: loadDetailMarketingChannelImage) {
                                ImagePicker(selectedImage: $selectedDetailMarketingChannelImage)
                            }
                            
                        }//VStack 브랜드 이미지
                        
                        
                        if modificationMode{
                            
                            TextField(".", text: $marketingChannelNameFor_DetailMarketingChannel, prompt: Text("\(marketingChannelNameFor_DetailMarketingChannel)")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.title2)
                            
                        }else {
                            
                            Text("\(marketingChannelNameFor_DetailMarketingChannel)")
                                .font(.title2)
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationMode){
                            Button(action: {
                                
                                if (!isApiLoading) {
                                    
                                    if (marketingChannelNameFor_DetailMarketingChannel == "" ||  managerNameFor_DetailMarketingChannel == "" || managerPhoneNumberFor_DetailMarketingChannel == "" || managerEmailFor_DetailMarketingChannel == "" || workAreaFor_DetailMarketingChannel == "" || channelImageFor_DetailMarketingChannel == ""){
                                        
                                        showAlert = true
                                        
                                        alertTitle = "마케팅 채널 수정 실패"
                                        alertMessage = "칸을 비울 수 없습니다."
                                        
                                    } else {
                                        patchMarketingChannel()
                                        
                                        modificationMode = false
                                    }
                                    
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationMode = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
                    .padding(.top, screenHeight / 81.5)
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("한줄설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            TextField("", text: $oneLineExplanFor_DetailMarketingChannel)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                            
//                            RightAlignedTextField(text: $oneLineExplanFor_DetailMarketingChannel)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(oneLineExplanFor_DetailMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("담당자명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            TextField("", text: $managerNameFor_DetailMarketingChannel)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                            
//                            RightAlignedTextField(text: $managerNameFor_DetailMarketingChannel)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(managerNameFor_DetailMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("담당자 전화번호")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            TextField("", text: $managerPhoneNumberFor_DetailMarketingChannel)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                .keyboardType(.numberPad)
                            
//                            RightAlignedTextField(text: $managerPhoneNumberFor_DetailMarketingChannel)
//                                .keyboardType(.numberPad)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(managerPhoneNumberFor_DetailMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("담당자 이메일")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            
                            TextField("", text: $managerEmailFor_DetailMarketingChannel)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color("color00000040"))
                                .frame(height: screenHeight/40.5)
                                .autocapitalization(.none)
                                .keyboardType(.emailAddress)
                            
//                            RightAlignedTextField(text: $managerEmailFor_DetailMarketingChannel)
//                                .keyboardType(.emailAddress)
//                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(managerEmailFor_DetailMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("업무영역")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        
                        if modificationMode{
                            Button(action: {
//                                selecteWorkAreaForDetailBrand = true
                            }, label: {
                                
                                //HStack 으로 버튼 세 개 만든 후 각각 터치 이벤트에 selecteWorkAreaForDetailBrand = false 처리하기
                                
                                
                                
                                
                                
                                if workAreaModificationModeFor_DetailMarketingChannel{
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailMarketingChannel = false
                                        
                                        workAreaFor_DetailMarketingChannel = "광고마케팅"
                                        
                                    }, label: {
                                        Text("광고마케팅")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailMarketingChannel = false
                                        
                                        workAreaFor_DetailMarketingChannel = "MCN"
                                        
                                    }, label: {
                                        Text("MCN")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailMarketingChannel = false
                                        
                                        workAreaFor_DetailMarketingChannel = "기타"
                                        
                                    }, label: {
                                        Text("기타")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    
                                }else{
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailMarketingChannel = true
                                        
                                    }, label: {
                                        Text("\(workAreaFor_DetailMarketingChannel)")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                }
                                
                                
                                
                                
                            })
                        } else{
                            
                            Text("\(workAreaFor_DetailMarketingChannel)")
                                .foregroundColor(.white)
                                .bold()
                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                .background(Color("mainColor"))
                                .cornerRadius(16.0)
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    VStack(spacing: 0) {
                        HStack{
                            
                            Text("계약 리스트")
                                .font(.title2)
                                .bold()
                                .padding()
                            //                                .padding(.top, -screenHeight/13.5)
                            Spacer()
                        }
                        .padding(0)
                        
                        Button(action: {
                            print("계약 추가 버튼 클릭됨")
                            
                            self.navigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddContractOfMarketingChannelView(selectedMarketingChannelIdx: selectedMarketingIdx, selectedDetailMarketingChannelName: marketingChannelNameFor_DetailMarketingChannel, selectedMarketingIdx: selectedMarketingIdx), isActive: $navigate) {
                                EmptyView()
                            }
                            
                            Text("+  계약 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        
                        
                        VStack{

                            ForEach(channelContractDataList.prefix(showAllContractList ? channelContractDataList.count : 3), id: \.marketingContractIdx) { channelContractData in
                            
//                            ForEach(Array(contractDataList.prefix(showAllContractList ? contractDataList.count : 3)), id: \.brandContractIdx) { contractData in
                                
                                NavigationLink(destination: ContractDetailOfMarketingChannelView(selectedMarketingChannelContractIdx: channelContractData.marketingContractIdx, selectedMarketingIdx: selectedMarketingIdx)) {
                                    
                                    VStack{
                                        HStack{ //리스트항목의 모델
                                            
                                            if channelContractData.contractImage != nil {
                                                
                                                AsyncImage(url: URL(string: imageS3Url + "/" + channelContractData.contractImage)) { image in
                                                    image.resizable()
                                                         .aspectRatio(contentMode: .fill)
                                                         .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                         .cornerRadius(12.0)
                                                } placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                }
                                                
                                            } else {
                                                
                                                Rectangle()
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                            }
                                            
                                            VStack(alignment: .leading){
                                                Text(channelContractData.contractName)
                                                    .foregroundColor(.black)
                                                
                                                Text("\(dateFormatter(getDate: channelContractData.contractStartDt)) ~ \(dateFormatter(getDate: channelContractData.contractEndDt))")
                                                    .foregroundColor(Color("color00000040"))
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            
                                            Text(channelContractData.contractStatus)
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                            
                                            
                                        }
                                        
                                        Divider()
                                            .padding(.leading, screenWidth/8)
                                    }
                                    
                                }//NavigationLink
                            }
                            
                            
                            
                            
                        } //VStack 계약 리스트//VStack 계약 리스트
                        .listStyle(PlainListStyle())
                        .padding(.horizontal)
                        .padding(.top, screenHeight/50.9375)
                        
                        
                        if showShowAllButton{
                            
                            Button(action: {
                                
                                showShowAllButton = false
                                showAllContractList = true
                                
                            }, label: {
                                
                                Text("더 보기")
                                    .foregroundColor(.black)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                            })
                            .background(.white)
                            .cornerRadius(4)
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color(.black), lineWidth: 1)
                                .padding()
                            )
                            
                        }
                       
                        
                        
                        
                        
                        
                        
                    }//VStack
                    .frame(width: screenWidth/1.0932)
                    .background(Color.white)
                    .cornerRadius(20)
                    .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                    .padding(.top, screenHeight/31.346)
                    
                    //                    .sheet(isPresented: $showingBankRecordCopyImagePickerForDetailBrand, onDismiss: loadContractImageForDetailBrandContractList) {
                    //                        ImagePicker(selectedImage: $selectedBankRecordCopyImageForDetailBrand)
                    //                    }
                    
                    
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/8.826)
                    
                    Spacer()
                    
                } //VStack
                
            }//ScrollView
        }//ZStack
        .alert(isPresented: $showAlert, content: {
            Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("확인"), action: {
                print("알림창 확인 버튼이 클릭되었다.")
                showAlert = false
                
                alertTitle = ""
                alertMessage = ""
            }))
        })
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            self.presentationMode.wrappedValue.dismiss()
        }) {
            HStack(spacing: 0){
                Image(systemName: "chevron.backward")
            }
        })
        .navigationBarTitle("\(marketingChannelNameFor_DetailMarketingChannel)", displayMode: .inline)
        .onAppear {
            marketingInquiry()
            contractInquiry(accessToken: loginData.token)
            
        }
        
        
        
        
    }//body
    
    private func loadDetailMarketingChannelImage() {
        guard let selectedDetailMarketingChannelImage = selectedDetailMarketingChannelImage else { return }
        // You can do something with the selected brand image here
        
        isApiLoading = true
        
        uploadImage(image: selectedDetailMarketingChannelImage, imageType: "marketing")
    }
    
    private func loadContractImageForDetailMarketingChannelContractList() {
        guard let selectedBankRecordCopyImageFor_DetailMarketingChannel = selectedBankRecordCopyImageFor_DetailMarketingChannel else { return }
        // You can do something with the selected brand image here
    }
    
    private func marketingInquiry() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/marketing?marketing_idx=\(selectedMarketingIdx)", method: .get, headers: headers).responseDecodable(of: MarketingDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                for marketingData in response.data {
                    channelImageFor_DetailMarketingChannel = marketingData.representativeImage
                    marketingChannelNameFor_DetailMarketingChannel = marketingData.channelName
                    oneLineExplanFor_DetailMarketingChannel = marketingData.oneLineExplain
                    managerNameFor_DetailMarketingChannel = marketingData.managerName
                    managerPhoneNumberFor_DetailMarketingChannel = marketingData.managerPhone
                    managerEmailFor_DetailMarketingChannel = marketingData.managerEmail
                    workAreaFor_DetailMarketingChannel = marketingData.manageType
                }
                
                
                //                    for brandData in response.data {
                //                         // 브랜드 이름을 출력합니다.
//                                        print("브랜드 번호: \(brandData.brandIdx)")
                //                        print("브랜드 사진: \(brandData.brandImage)")
                //                        print("브랜드 이름: \(brandData.brandName)")
                //                        print("비즈니스 타입: \(brandData.buisnessType)")
                //                        print("등록일: \(brandData.firstCreateDt)")
                //
                //                    }
                
                print(response.data)
                
//                brandIdx = brandData.brandIdx
//                DispatchQueue.main.async {
//                    marketingDataList = response.data
//                }
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
        
        
    }
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonMarketingImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "mimetype": "image/png",
            "type": "marketing",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonMarketingImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                    
                    isApiLoading = false
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "marketing":
                        channelImageFor_DetailMarketingChannel = key!
                        
                        print("\(channelImageFor_DetailMarketingChannel)")
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    //최종적으로 수정
    private func patchMarketingChannel() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            "marketing_idx": selectedMarketingIdx,
              "channel_name": marketingChannelNameFor_DetailMarketingChannel,
              "one_line_explain": oneLineExplanFor_DetailMarketingChannel,
              "manager_name": managerNameFor_DetailMarketingChannel,
              "manager_phone": managerPhoneNumberFor_DetailMarketingChannel,
              "manager_email": managerEmailFor_DetailMarketingChannel,
              "manage_type": workAreaFor_DetailMarketingChannel,
              "representative_image": channelImageFor_DetailMarketingChannel
        ]
        
        
        AF.request("\(defaultUrl)/admin/api/marketing/\(selectedMarketingIdx)",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("고객 브랜드 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("고객 브랜드 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("고객 브랜드 수정 실패: \(error)")
                
            }
        }
    }
    
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormatter.string(from: date)
            print(dateString) // "2024.01.03" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }

    }
    
    
    //계약 가져오는 api
    func contractInquiry(accessToken: String) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(accessToken)",
                "Accept": "application/json"
            ]
            
            AF.request("\(defaultUrl)/admin/api/marketing-contract?marketing_idx=\(selectedMarketingIdx)", method: .get, headers: headers).responseDecodable(of: ChannelContractDataResponse.self) { response in
                switch response.result {
                case .success(let response):
                    // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                    
                    
                    DispatchQueue.main.async {
                        channelContractDataList = response.data
                        
                        channelContractCount = response.pagination.total
                        
                        print("계약 리스트 호출 성공 \(response.data)")
                        
                        if (channelContractCount <= 3) { //
                            showShowAllButton = false
                            showAllContractList = false
                        }else{
                            showShowAllButton = true //더 보기 버튼 보임
                            showAllContractList = false
                        }
                    }
                    
                    
                    
                    
                case .failure(let error):
                    // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                    print(error)
                }
            }
            
        })
        
    }
    
}
//
//#Preview {
//    DetailMarketingChannelView()
//}
