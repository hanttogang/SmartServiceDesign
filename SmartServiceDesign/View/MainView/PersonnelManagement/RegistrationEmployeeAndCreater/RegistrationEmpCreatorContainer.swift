//
//  RegistrationEmpCreatorContainer.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/11/24.
//

import SwiftUI
import Alamofire

enum UserTypeInRegistrationECView: String, CaseIterable, Identifiable {
    case employee = "employee"
    case creator = "creator"
    
    var id: Self { self }
}

private enum RegistrationCreatorWorkArea: CaseIterable {
    case advertisingMarketing
    case mcn
    case anotherExample
}

struct RegistrationEmpCreatorContainer: View {
    
    //Api
    @EnvironmentObject var searchViewModel: SearchViewModel
    @EnvironmentObject var loginData: LoginData
    
    @State private var userList = [User]()
    let defaultUrl = "\(ApiClient.BASE_URL)"
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var selectedUserType: UserTypeInRegistrationECView = UserTypeInRegistrationECView.employee
    @State private var userTypeText: String = ""
    
    @State private var userNameFor_CreatorManagement: String = ""
    
    @State private var userOneLineExplanFor_CreatorManagement: String = ""
    @State private var userPhoneNumberFor_CreatorManagement: String = ""
    @State private var userEmailFor_CreatorManagement: String = ""
    
    @State private var showingUserImagePickerFor_CreatorRegistration = false
    @State private var selectedUserImageFor_CreatorRegistration: UIImage? = nil
    
    
    @State private var selectedRegistrationCreatorWorkArea: RegistrationCreatorWorkArea = .advertisingMarketing
    
    @State private var navigate: Bool = false
    
    
    @State private var searchUserName: String = ""
    @State private var editText: Bool = false
    
    //    @State var navigateECUserSearchView: Bool = false
    
    
    
    var body: some View {
        
        
        
        if !searchViewModel.navigateECUserSearchView{
            ZStack{
                
                
                VStack {
                    
                    
                    HStack(spacing: 0) {
                        ForEach(UserTypeInRegistrationECView.allCases, id: \.self) { flavor in
                            Button(action: {
                                selectedUserType = flavor
                                userTypeText = "\(flavor)"
                                print("\(flavor)")
                                
                                searchViewModel.userName = ""
                                searchViewModel.userPhoneNum = ""
                                searchViewModel.userEmail = ""
                            }) {
                                Text(flavor == .employee ? "소속직원" : "크리에이터")
                                    .frame(width: screenWidth/2.272, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                                
                            }
                            .background(selectedUserType == flavor ? Color.blue : Color.clear)
                            .foregroundColor(selectedUserType == flavor ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            
                            
                            
                        }
                    }
                    .overlay(RoundedRectangle(cornerRadius: 4)
                        .stroke(Color("mainColor"), lineWidth: 1)
                    )
                    .padding(.top, screenHeight / 25.46875) //32
                    
                    
                    
                    
                    
                    
                    VStack{
                        
                        if (selectedUserType == .employee){
                            RegistrationEmployeeView()
                        }else if(selectedUserType == .creator){
                            RegistrationCreatorView()
                        }
                        
                    }
                    .navigationBarBackButtonHidden(true)
                    
                }//VStack
                        
                        
                    
                    
                
                
            }//ZStack
            
            
            
        }else{
            if(searchViewModel.userType == "employee"){
                
                ZStack{
                    VStack(spacing: 0){
                        
                        
                        HStack{
                            
                            HStack{
                                
                            }
                            .frame(width: screenWidth, height: screenHeight/14.5)
                            .overlay{
                                
                                HStack{
                                    
                                    
                                    TextField("Search" , text : self.$searchUserName)
                                        .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                        .foregroundColor(Color(.black))
                                        .padding(10)
                                        .padding(.leading, screenWidth/12.5) //30
                                        .background(Color(.white))
                                        .frame(width: screenWidth/1.3)
                                        .cornerRadius(15)
                                        .overlay(
                                            
                                            HStack{
                                                Image(systemName: "magnifyingglass")
                                                    .foregroundColor(Color("hintTextColor"))
                                                    .padding()
                                                
                                                Spacer()
                                                
                                                if self.editText{
                                                    Button(action : {
                                                        self.editText = false
                                                        self.searchUserName = ""
                                                        
                                                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                                    }){
                                                        Image(systemName: "multiply.circle.fill")
                                                            .foregroundColor(Color("hintTextColor"))
                                                            .padding()
                                                    }
                                                }
                                                
                                            }//HStack
                                            
                                        )//overlay
                                        .onTapGesture {
                                            self.editText = true
                                        }
                                    
                                    Button(action: {
                                        searchViewModel.navigateECUserSearchView = false
                                        
                                    }, label: {
                                        Text("취소")
                                    })
                                    .padding(.horizontal, screenWidth/46.875)
                                    //                            }
                                    
                                    
                                }
                            }//overlay
                        }//HStack 검색창
                        .background(Color("colorF8F8F892"))
                        
                        HStack{
                            
                            Text("직원 검색")
                                .font(.title)
                                .padding(.leading, screenWidth/23.4375) //16
                                .padding(.top, screenWidth/20.833) //18
                            
                            Spacer()
                        }
                        Divider() // 가로 선
                            .padding(.top, 8)
                            .padding(.leading, screenWidth/23.4375) //16
                        
                        
                        ScrollView {
                            
                            VStack(alignment: .leading, spacing: 0) {
                                ForEach(userList.filter { searchUserName.isEmpty || $0.userName.contains(searchUserName) }, id: \.userIdx) { user in
                                    
                                    Button {
                                        searchViewModel.userIdx = user.userIdx
                                        
                                        searchViewModel.userName = user.userName
                                        searchViewModel.userPhoneNum = user.userPhone
                                        searchViewModel.userEmail = user.userEmail
                                        
                                        searchViewModel.navigateECUserSearchView = false
                                    } label: {
                                        HStack(){
                                            Text(user.userName)
                                                .frame(minWidth: 0, minHeight: 0, maxHeight: .infinity)
                                                .foregroundColor(.blue)
                                            
                                        }
                                        .padding()
                                    }
                                    
                                    Divider()
                                        .padding(.leading, screenWidth/23.4375) //16
                                    
                                    
                                    
                                }
                            }
                            
                            
                        }
                        .background(.white)
                        .onAppear{
                            userInquiry()
                        }
                        
                        
                        
                    } //VStack
                    .navigationBarHidden(true)
                    
                    
                    
                }//ZStack 검색 뷰
                .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                    UIApplication.shared.endEditing()
                    
                }
                .background(.white)
                
                //Employee 검색 뷰
            }
            else{
                
                ZStack{
                    VStack(spacing: 0){
                        
                        
                        HStack{
                            
                            HStack{
                                
                            }
                            .frame(width: screenWidth, height: screenHeight/14.5)
                            .overlay{
                                
                                HStack{
                                    
                                    
                                    TextField("Search" , text : self.$searchUserName)
                                        .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                        .foregroundColor(Color(.black))
                                        .padding(10)
                                        .padding(.leading, screenWidth/12.5) //30
                                        .background(Color(.white))
                                        .frame(width: screenWidth/1.3)
                                        .cornerRadius(15)
                                        .overlay(
                                            
                                            HStack{
                                                Image(systemName: "magnifyingglass")
                                                    .foregroundColor(Color("hintTextColor"))
                                                    .padding()
                                                
                                                Spacer()
                                                
                                                if self.editText{
                                                    Button(action : {
                                                        self.editText = false
                                                        self.searchUserName = ""
                                                        
                                                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                                    }){
                                                        Image(systemName: "multiply.circle.fill")
                                                            .foregroundColor(Color("hintTextColor"))
                                                            .padding()
                                                    }
                                                }
                                                
                                            }//HStack
                                            
                                        )//overlay
                                        .onTapGesture {
                                            self.editText = true
                                        }
                                    
                                    Button(action: {
                                        searchViewModel.navigateECUserSearchView = false
                                        
                                    }, label: {
                                        Text("취소")
                                    })
                                    .padding(.horizontal, screenWidth/46.875)
                                    //                            }
                                    
                                    
                                }
                            }//overlay
                        }//HStack 검색창
                        .background(Color("colorF8F8F892"))
                        
                        HStack{
                            
                            Text("크리에이터 검색")
                                .font(.title)
                                .padding(.leading, screenWidth/23.4375) //16
                                .padding(.top, screenWidth/20.833) //18
                            
                            Spacer()
                        }
                        Divider() // 가로 선
                            .padding(.top, 8)
                            .padding(.leading, screenWidth/23.4375) //16
                        
                        ScrollView {
                            
                            VStack(alignment: .leading, spacing: 0) {
                                ForEach(userList.filter { searchUserName.isEmpty || $0.userName.contains(searchUserName) }, id: \.userIdx) { user in
                                    
                                    Button {
                                        searchViewModel.userIdx = user.userIdx
                                        
                                        searchViewModel.userName = user.userName
                                        searchViewModel.userPhoneNum = user.userPhone
                                        searchViewModel.userEmail = user.userEmail
                                        
                                        searchViewModel.navigateECUserSearchView = false
                                    } label: {
                                        HStack(){
                                            Text(user.userName)
                                                .frame(minWidth: 0, minHeight: 0, maxHeight: .infinity)
                                                .foregroundColor(.blue)
                                            
                                        }
                                        .padding()
                                    }
                                    
                                    Divider()
                                        .padding(.leading, screenWidth/23.4375) //16
                                    
                                }
                            }
                        }
                        .background(.white)
                        .onAppear{
                            userInquiry()
                        }
                        
                    } //VStack
                    .navigationBarHidden(true)
                    
                    
                    
                }//ZStack 검색 뷰
                .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                    UIApplication.shared.endEditing()
                    
                }//Creator 검색 뷰
                .background(.white)
            }
            
        }//else 직원/크리에이터 검색
        
        
    }
    
    private func loadUserImageForCreatorRegistration() {
        guard let selectedUserImageFor_CreatorRegistration = selectedUserImageFor_CreatorRegistration else { return }
        // You can do something with the selected brand image here
    }
    
    
    
    //계약 가져오는 api
    func userInquiry() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/admin/api/user?user_type=\(searchViewModel.userType)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: UserDataResponse.self) { response in
            switch response.result {
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                
                
                DispatchQueue.main.async {
                    userList = response.data
                    
                    print("계약 리스트 호출 성공 \(response.data)")
                    
                }
                
                
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
}

//#Preview {
//    RegistrationEmpCreatorContainer().environmentObject(SearchViewModel())
//}
