////
////  SoketResponse.swift
////  SmartServiceDesign
////
////  Created by Teameverywhere on 1/25/24.
////
//
//
//import Foundation
//
//enum ResponseType: String, Codable {
//    case connect = "CONNECT"
//    case message = "MESSAGE"
//}
//
//enum ChatType: String, Codable{
//    case text = "TEXT"
//    case file = "FILE"
//    case date = "DATE"
//    case alert = "ALERT"
//    case image = "IMAGE"
//}
//
//enum UserType: String, Codable {
//    case client = "CLIENT"
//    case manager = "MANAGER"
//}
//
//
//struct ResponseConnectModel: Codable {
//    let result: ResultStatusModel
//    let transaction: ResponseType
//    let timestamp: Int
//    let channel:String?
//    let channelId: String?
//    let message: MessageDataModel
//    let history: [HistoryDataModel]?
//    let user: UserDataModel
//    let tenant: TenantDataModel?
//}
//
//struct ResultStatusModel: Codable {
//    let code: Int
//    let message: String
//}
//
//struct MessageDataModel: Codable {
//    let seq: Int
//    let type: ChatType
//    let content: String
//    let date: String
//    let time: String
//    let createdAt: Int
//}
//
//struct HistoryDataModel: Codable, Equatable {
//    let user: UserDataModel
//    let seq: Int
//    let type: ChatType
//    let content: String
//    let date: String
//    let time: String
//    let createdAt: Int
//    static func ==(lhs: HistoryDataModel, rhs: HistoryDataModel) -> Bool {
//        return lhs.seq == rhs.seq // Just an example, compare whatever properties you want.
//    }
//    
//}
//
//struct UserDataModel: Codable {
//    let id: String
//    let nickname: String?
//    let type: UserType
//    let profileUrl: String?
//}
//
//struct TenantDataModel: Codable {
//    let id: String
//    let name: String
//    let ceo: String
//    let phone: String
//    let email: String
//    let profileUrl: String?
//}
