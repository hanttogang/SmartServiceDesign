//
//  CalendarView.swift
//  CalendarTest
//
//  Created by 김동균 on 2023/12/13.
//
import SwiftUI

struct Event: Identifiable {
    let id = UUID()
    var title: String
    var startDate: Date
    var endDate: Date
    var color: Color
}

struct Day: Identifiable{
    let id = UUID()
    var date: Date
    var events: [Event]
    var isEmptyDay: Bool
}

struct DayView: View {
    var day: Day
    var events: [Event]

    var body: some View {
        ZStack(alignment: .topTrailing) {
            VStack(spacing:0) {
                Text("\(Calendar.current.component(.day, from: day.date))")
                    .frame(maxWidth: .infinity)
                    .padding(.top, 12)
                Spacer()

                ForEach(events) { event in
                    if event.startDate == day.date {
                        Rectangle()
                            .fill(event.color)
                            .frame(maxWidth: .infinity, maxHeight: 24)
                            .padding(.horizontal, isStartOrEnd(event: event) ? 0 : 8)
                            .cornerRadius(12, corners: [.topLeft, .bottomLeft])
                            
                    } else if event.endDate == day.date {
                        Rectangle()
                            .fill(event.color)
                            .frame(maxWidth: .infinity, maxHeight: 24)
                            .padding(.horizontal, isStartOrEnd(event: event) ? 0 : 8)
                            .cornerRadius(12, corners: [.topRight, .bottomRight])
                            
                    } else if event.startDate < day.date && event.endDate > day.date {
                        Rectangle()
                            .fill(event.color)
                            .frame(maxWidth: .infinity, maxHeight: 24)
                            .padding(.horizontal, 0)
                    }
                }
            }
            .padding(.bottom, 8)
        }
        .background(Color.white)
        .border(Color.gray.opacity(0.2))
        .frame(minHeight: 54 + CGFloat(events.count * 24) + 12) // Adjust height based on number of events
    }

    private func isStartOrEnd(event: Event) -> Bool {
        return event.startDate == day.date || event.endDate == day.date
    }
}

struct WeekView: View {
    var days: [Day]
    var events: [Event]
    
    var body: some View {
        HStack(spacing: 0) {
            ForEach(days) { day in
                DayView(day: day, events: eventsForDay(day: day))
            }
        }
    }

    private func eventsForDay(day: Day) -> [Event] {
        events.filter { $0.startDate <= day.date && $0.endDate >= day.date }
    }
}

struct MonthView: View {
    var days: [Day]
    var events: [Event]
    
    // Compute weeks
    private var weeks: [[Day]] {
        days.chunked(into: 7)
    }
    
    var body: some View {
        VStack(spacing: 0) {
            ForEach(Array(weeks.enumerated()), id: \.offset) { _, week in
                    WeekView(days: week, events: events)
            }
        }
    }
}

// Sample View for demonstration
struct CalendarView: View {
    let calendar = Calendar.current

    // Events are declared outside of the `days` computed property so they are in scope for `MonthView`.
    let event1 = Event(title: "Event 1",
                       startDate: Calendar.current.date(from: DateComponents(year: 2023, month: 1, day: 5))!,
                       endDate: Calendar.current.date(from: DateComponents(year: 2023, month: 1, day: 10))!,
                       color: .blue)
    let event2 = Event(title: "Event 2",
                       startDate: Calendar.current.date(from: DateComponents(year: 2023, month: 1, day: 8))!,
                       endDate: Calendar.current.date(from: DateComponents(year: 2023, month: 1, day: 17))!,
                       color: .green)
    let event3 = Event(title: "Event 3",
                       startDate: Calendar.current.date(from: DateComponents(year: 2023, month: 1, day: 8))!,
                       endDate: Calendar.current.date(from: DateComponents(year: 2023, month: 1, day: 17))!,
                       color: .red)
    
    // Generate a list of days for the month with events
    var days: [Day] {
            var days: [Day] = []
            let currentMonth = calendar.date(from: DateComponents(year: 2023, month: 1))!
            let range = calendar.range(of: .day, in: .month, for: currentMonth)!
            let startOfMonth = calendar.date(from: calendar.dateComponents([.year, .month], from: currentMonth))!
            
            // Calculate the weekday of the first day of the month
            let weekdayOfFirst = calendar.component(.weekday, from: startOfMonth)
            
            // Add leading placeholder days from the previous month
            for day in (0..<(weekdayOfFirst - 1)).reversed() {
                if let date = calendar.date(byAdding: .day, value: -day, to: startOfMonth) {
                    days.append(Day(date: date, events: [], isEmptyDay: true))
                }
            }
            
            // Add actual days of the month
            for day in range {
                let date = calendar.date(byAdding: .day, value: day - 1, to: startOfMonth)!
                var eventsForDay = [Event]()
                
                // ... (event checking logic)

                days.append(Day(date: date, events: eventsForDay, isEmptyDay: false))
            }

            // Add trailing placeholder days to complete the last week
            let numberOfDaysInLastWeek = 7 - days.count % 7
            if numberOfDaysInLastWeek < 7 {
                for day in 1...numberOfDaysInLastWeek {
                    if let date = calendar.date(byAdding: .day, value: day, to: days.last!.date) {
                        days.append(Day(date: date, events: [], isEmptyDay: true))
                    }
                }
            }

            return days
        }
    
    var body: some View {
        ScrollView {
            MonthView(days: days, events: [event1, event2, event3, event3, event3, event3])
        }
    }
}
extension Array {
    func chunked(into size: Int) -> [[Element]] {
        stride(from: 0, to: count, by: size).map {
            Array(self[$0..<Swift.min($0 + size, count)])
        }
    }
}

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}

// Define `RoundedCorner` shape
struct RoundedCorner: Shape {
    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
