//
//  CardView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/19/23.
//

import SwiftUI

struct CardView: View {
    
    
    var body: some View {
            VStack {
                Text("Hello, World!")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding()

                Button(action: {}) {
                    Text("Click Me")
                        .foregroundColor(.white)
                        .padding()
                        .background(Color.blue)
                        .cornerRadius(10)
                }
            }
            .frame(width: 300, height: 200)
            .background(Color.white)
            .cornerRadius(20)
            .shadow(color: .gray, radius: 10, x: 0, y: 10)
        }
    
    
}

#Preview {
    CardView()
}
