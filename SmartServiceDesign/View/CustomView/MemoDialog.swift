//
//  MemoDialog.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/20/23.
//

import SwiftUI

struct MemoDialog: View {
    @Environment(\.presentationMode) var presentationMode
    
    
    
    @State var navigateToLoginView = false
    
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State var memo:String = ""
    
    var body: some View {
        
        ZStack{//메모 뷰
                
            VStack{
                Text("메모 작성")
                    .padding(.vertical, screenHeight/74.09)
                    .bold()
                
                Divider()
                
                Spacer()
                
                VStack{
                    TopAlignedTextField(text: $memo)
                        .frame(width: screenWidth/1.2, height: screenHeight/8.15)
                        .autocapitalization(.none)
                        .foregroundColor(Color("hintTextColor"))
                    
                }
                
                Spacer()
                
                Divider()
                
                HStack{
                    Spacer()
                    
                    Button(action: {
                        
                    }, label: {
                        
                        Text("등록")
                            .bold()
                            .foregroundColor(.black)
                        
                        Image(systemName: "chevron.right")
                            .foregroundColor(.black)
                    })
                }
                .padding(.vertical, screenHeight/74.09)
                .padding(.trailing, screenWidth/25)
                
            }
            .frame(width: screenWidth/1.0932, height: screenHeight/3.844)
            .background(.colorF8F8F892)
            .cornerRadius(8.0)
            
        }//ZStack
        
        
    }
}

#Preview {
    MemoDialog()
}
