//
//  CustomBottomNavigationBar.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/14/23.
//

import SwiftUI
//import WebKit

struct CustomBottomNavigationBar: View {
    
    @EnvironmentObject var myWebVM: WebViewModel

    @State var jsValue: JsValue?
    
    @EnvironmentObject var loginData: LoginData
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var chatWebView: Bool = false
    @State private var myPageView: Bool = false
    
    @State private var isShowingMenu: Bool = false
    
    @State private var isApiLoading: Bool = false
    
    @State private var webViewTrigger: Bool = false
    
    var body: some View {
        
        
        ZStack{
            
//            if (isApiLoading){
//                
//                ProgressView()
//                    .zIndex(3)
//                
//            }
            
            if(chatWebView){
                //웹 페이지에서 보여질 상단 네비게이션 작성하기
                VStack(spacing: 0){
                    
                    
//                    
//                    HStack{
//                        
//                        Button(action: {
//                            withAnimation {
//                                self.isShowingMenu = true
//                            }
//                        }) {
//                            
//                            HStack(spacing: 0){
//                                Image("img_menu")
//                                    .padding(.bottom)
//                            }
//                            .padding(.leading)
//                            
//                        }
//                        
//                        
//                        Spacer()
//                        
//                    }
//                    .background(.colorF8F8F8)
//                        
                    
                   
                    
                    VStack{

                        if(webViewTrigger){
                            MyWebView(urlToLoad: "https://smart-service-web.team-everywhere.com/chat?token=\(loginData.token)")
                        }else{
                            MyWebView(urlToLoad: "https://smart-service-web.team-everywhere.com/chat?token=\(loginData.token)")
                        }
                        
                        
                        
                    }
//                    //onReceive : 이벤트를 받는다
//                    //myWebVM.jsAlertEvent 을 받고
//                    //perform 을 보내준다.
                    .onReceive(myWebVM.jsBridgeEvent, perform: { jsValue in
                        
                        
                        print("채팅 contentView - jsValue", jsValue)
                        
                        let decoder = JSONDecoder()
                           if let data = jsValue.message.data(using: .utf8),
                              let message = try? decoder.decode(ChatAlertBridgeMessage.self, from: data) {
                               if message.idx == "back" {
                                   // 'idx'의 값이 'back'인 경우에만 실행할 코드를 이곳에 작성합니다.
                                   print("Back event received")
                                   
                                   webViewTrigger.toggle()
                                   
                                   
                               }else if message.idx == "alert" {
                                   // 'idx'의 값이 'alert'인 경우에만 실행할 코드를 이곳에 작성합니다.
                                   print("Alert event received")
                                   
                                   self.jsValue = jsValue
                               }
                               
                               
                           }
                        
                                        
                        
                        
//                        let decoder = JSONDecoder()
//                        if let data = jsValue.message.data(using: .utf8),
//                           
//                           let message = try? decoder.decode(ChatAlertBridgeMessage.self, from: data) {
//                            
//                              
//                        }
                    })
                    .alert(item: $jsValue, content: { alert in
                        createAlert(alert)
                    })
                    
                    
                    
                    
                    
                    
                    
                    //하단 네비게이션을 위한 패딩
                    HStack{
                        Rectangle()
                            .frame(width: 0, height: 0)
                            .foregroundColor(.white)
                    }
                    .padding(.bottom, screenHeight/11.6) //70
                        
                    
                }

            }else if(myPageView){
                MyPageView()
            }
            
            VStack(spacing: 0){
                
                
                Spacer()
                
                
                HStack{
                    
                    
                    Button(action: {
                        
//                        isApiLoading = chatWebView
                        print("메시지 관리 클릭")

                        myPageView = false
                        chatWebView.toggle()
                        
                    }, label: {
                        
                        
                        VStack() {
                            Image(systemName: "list.bullet")
                                .foregroundColor(chatWebView ? .blue : .gray)
                                .imageScale(.large)
                            
                            
                            
                            Text("메세지 관리")
                            
                                .foregroundColor(chatWebView ? .blue : .gray)
                                .padding(.vertical, 4)
                            
                            
                        }
                        .frame(width: screenWidth/2, height: screenHeight/13.588)
                        
                    })
                    .padding(.top, 4)
                    
                    Spacer()
                    
                    
                    Button {
                        print("마이페이지 클릭")
                        chatWebView = false
                        myPageView.toggle()
                    } label: {
                        VStack() {
                            Image(systemName: "person.crop.circle")
                                .foregroundColor(myPageView ? .blue : .gray)
                                .imageScale(.large)
                            
                            
                            
                            Text("마이 페이지")
                                .foregroundColor(myPageView ? .blue : .gray)
                                .padding(.vertical, 4)
                            
                            
                        }
                        .frame(width: screenWidth/2, height: screenHeight/13.588)
                    
                    }
                    .padding(.top, 4)
                    
                    
                }//HStack
                .background(.colorF8F8F8)
                
            }//VStack
            .padding(.vertical, screenHeight/73.818)
            
            
            // isShowingMenu 상태에 따라 메뉴 여닫기 zIndex 으로 조정
            if isShowingMenu {
                
                MenuView()
                    .transition(.move(edge: .leading))
                    .zIndex(2)
                
                // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                Button(action: {
                    withAnimation {
                        self.isShowingMenu = false
                    }
                }) {
                    Color.gray
                        .edgesIgnoringSafeArea(.all)
                        .opacity(0.5)
                }
                .zIndex(1)
                
            }
            
        }//ZStack
       
        .edgesIgnoringSafeArea(.bottom)
        
        
        
    }//body
}


extension CustomBottomNavigationBar {
    
    func createAlert(_ alert: JsValue) -> Alert {
//        Alert(title: Text(alert.type.description), message: Text(alert.message), dismissButton: .default(Text("확인"), action: {
        Alert(title: Text(alert.type.description), message: Text("이미 존재하는 채팅방입니다."), dismissButton: .default(Text("확인"), action: {
            print("알림창 확인 버튼이 클릭되었다.")
        }))
    }
//    
//    //Text 입력 얼럿 창
//    func createTextAlert() -> MyTextAlertView {
//        MyTextAlertView(textString: $textString, showAlert: $shouldShowAlert, title: "iOS -> Js 보내기", message: "")
//    }
}


struct ChatAlertBridgeMessage: Decodable {
    let idx: String
    let title: String
}

//
//#Preview {
//    CustomBottomNavigationBar()
//}
