//
//  SplashView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//

import SwiftUI

struct SplashView: View {
    var body: some View {
        
        GeometryReader{ geometry in
            
                VStack{
                    Spacer()
                    HStack{
                        Spacer()
                        Image("imgLogo")
                        Spacer()
                    }
                    Spacer()
                }
                
        }
        .background(.white)
        
        
        
    }
}

#Preview {
    SplashView()
}
