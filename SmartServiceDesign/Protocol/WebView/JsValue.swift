//
//  JSValue.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/31/24.
//

import Foundation

struct JsValue: Identifiable{
    
    enum TYPE: CustomStringConvertible{
        case JS_ALERT, JS_BRIDGE/*, BLOCKED_SITE*/
        
        var description: String {
            switch self {
            case .JS_ALERT: return "JS_ALERT 타입"
            case .JS_BRIDGE: return "채팅방 만들기 실패"
//            case .BLOCKED_SITE: return "차단된 사이트"
            }
        }
    }
    
    let id: UUID = UUID()
    var message: String = ""
    
    var type: TYPE
    
    init(_ message: String? = nil, _ type: TYPE) {
        self.message = message ?? "메세지 없음"
        self.type = type
    }
    
}
