//
//  WebSocketManager.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/25/24.
//

import Foundation
import Starscream
import UIKit


enum WebSocketEvents {
    case connected(headers: [String: String])
    case disconnected(reason: String, code: Int)
    case text(string: String)
    case binary(data: Data)
    case ping
    case pong
    case viabilityChanged
    case reconnectSuggested
    case cancelled
    case error(error: Error?)
    case peerClosed
}

class WebSocketManager: WebSocketDelegate {
    
    var socket: WebSocket!
    var delegate: WebSocketManagerDelegate?
    
    let loginData = LoginData()
    
    init(token: String) {
        var request = URLRequest(url: URL(string: "https://smart-service-socket.team-everywhere.com")!)
        request.timeoutInterval = 5
        request.addValue(token, forHTTPHeaderField: "\(loginData.token)")
        socket = WebSocket(request: request)
        socket.delegate = self
    }
    
    func connect() {
        socket.connect()
    }
    
    func disconnect() {
        socket.disconnect()
    }
    
    func send(message: String) {
        socket.write(string: message)
    }
    
    // Join a chat room
    func joinRoom(roomId: Int) {
        socket.write(string: "joinRoom")
    }
    
    // Create a chat room
    func createChatRoom(opponentUserIdx: Int) {
        socket.write(string: "createChatRoom")
    }
    
    // Get chat rooms
    func getChatRooms() {
        socket.write(string: "getChatRooms")
    }
    
    // Send a message
    func sendMessage(chatRoomIdx: Int, message: String) {
        let messageObject = ["chat_room_idx": chatRoomIdx, "message": ["type": "text", "content": message]] as [String : Any]
        if let messageData = try? JSONSerialization.data(withJSONObject: messageObject, options: []) {
            socket.write(data: messageData)
        }
    }
    
    // WebSocketDelegate methods
    func didReceive(event: Starscream.WebSocketEvent, client: Starscream.WebSocketClient) {
        switch event {
        case .connected(let headers):
            print("connected with headers: \(headers)")
            delegate?.didReceive(event: .connected(headers: headers))
        case .disconnected(let reason, let code):
            print("disconnected with reason: \(reason), code: \(code)")
            delegate?.didReceive(event: .disconnected(reason: reason, code: Int(code)))
        case .text(let string):
            print("received text: \(string)")
            delegate?.didReceive(event: .text(string: string))
        case .binary(let data):
            print("received data: \(data.count)")
            delegate?.didReceive(event: .binary(data: data))
        case .ping(_):
            delegate?.didReceive(event: .ping)
        case .pong(_):
            delegate?.didReceive(event: .pong)
        case .viabilityChanged(_):
            delegate?.didReceive(event: .viabilityChanged)
        case .reconnectSuggested(_):
            delegate?.didReceive(event: .reconnectSuggested)
        case .cancelled:
            delegate?.didReceive(event: .cancelled)
        case .error(let error):
            print("error: \(error?.localizedDescription ?? "")")
            delegate?.didReceive(event: .error(error: error))
        case .peerClosed:
            delegate?.didReceive(event: .peerClosed)
        }
    }
}

protocol WebSocketManagerDelegate {
    func didReceive(event: WebSocketEvents)
}


class ChatManager: WebSocketManagerDelegate, ObservableObject {
    var webSocketManager: WebSocketManager
    var messages: [String] = []
    
    let loginData = LoginData()

    init() {
        self.webSocketManager = WebSocketManager(token: loginData.token)
        self.webSocketManager.delegate = self
    }
    
    func sendChatMessage(_ message: String) {
        print("전송될 메시지: \(message)")
        self.webSocketManager.send(message: message)
    }
    
    func didReceive(event: WebSocketEvents) {
        print("didReceive called")
        
        switch event {
        case .text(let message):
            // 채팅 메시지를 받았을 때의 처리를 여기에 작성합니다.
            DispatchQueue.main.async {
                print("++++++++++메시지 배열에 더함 \(message)")
                self.messages.append(message)
            }
        default:
            break
        }
    }
    
    

}

/*
 
 // WebSocketDelegate methods
 func didReceive(event: Starscream.WebSocketEvent, client: Starscream.WebSocketClient) {
     switch event {
     case .connected(let headers):
         print("connected with headers: \(headers)")
         delegate?.didReceive(event: .connected(headers: headers))
     case .disconnected(let reason, let code):
         print("disconnected with reason: \(reason), code: \(code)")
         delegate?.didReceive(event: .disconnected(reason: reason, code: Int(code)))
     case .text(let string):
         print("received text: \(string)")
         delegate?.didReceive(event: .text(string: string))
     case .binary(let data):
         print("received data: \(data.count)")
         delegate?.didReceive(event: .binary(data: data))
     case .ping(_):
         delegate?.didReceive(event: .ping)
     case .pong(_):
         delegate?.didReceive(event: .pong)
     case .viabilityChanged(_):
         delegate?.didReceive(event: .viabilityChanged)
     case .reconnectSuggested(_):
         delegate?.didReceive(event: .reconnectSuggested)
     case .cancelled:
         delegate?.didReceive(event: .cancelled)
     case .error(let error):
         print("error: \(error?.localizedDescription ?? "")")
         delegate?.didReceive(event: .error(error: error))
     case .peerClosed:
         delegate?.didReceive(event: .peerClosed)
     }
 }
 
 */
