//
//  Contract.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/22/23.
//

import Foundation
import SwiftUI


struct Contract: Identifiable {
    var id = UUID()
    var image: UIImage?
    var repName: String
    var startDate: Date
    var endDate: Date
    var status: String
    
    var formattedStartDate: String {
        return formatter.string(from: startDate)
    }
    
    var formattedEndDate: String {
        return formatter.string(from: endDate)
    }
}

let formatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy.MM.dd"
    return formatter
}()


let contractDummyData = [
    Contract(image: UIImage(named: "Image1")!, repName: "계약1", startDate: formatter.date(from: "2023.01.01")!, endDate: formatter.date(from: "2023.01.31")!, status: "요청"),
    Contract(image: UIImage(named: "Image2")!, repName: "계약2", startDate: formatter.date(from: "2023.02.01")!, endDate: formatter.date(from: "2023.02.28")!, status: "진행"),
    Contract(image: UIImage(named: "Image3")!, repName: "계약3", startDate: formatter.date(from: "2023.03.01")!, endDate: formatter.date(from: "2023.03.31")!, status: "피드백"),
    Contract(image: UIImage(named: "Image4")!, repName: "계약4", startDate: formatter.date(from: "2023.04.01")!, endDate: formatter.date(from: "2023.04.30")!, status: "완료"),
    Contract(image: UIImage(named: "Image5")!, repName: "계약5", startDate: formatter.date(from: "2023.05.01")!, endDate: formatter.date(from: "2023.05.31")!, status: "보류"),
    Contract(image: UIImage(named: "Image2")!, repName: "계약5", startDate: formatter.date(from: "2023.05.02")!, endDate: formatter.date(from: "2023.05.30")!, status: "보류"),
    Contract(image: UIImage(named: "Image1")!, repName: "계약1", startDate: formatter.date(from: "2023.12.01")!, endDate: formatter.date(from: "2024.01.31")!, status: "요청"),
    Contract(image: UIImage(named: "Image2")!, repName: "계약2", startDate: formatter.date(from: "2023.11.01")!, endDate: formatter.date(from: "2024.02.28")!, status: "진행"),
    Contract(image: UIImage(named: "Image3")!, repName: "계약3", startDate: formatter.date(from: "2023.10.01")!, endDate: formatter.date(from: "2024.03.31")!, status: "피드백"),
    Contract(image: UIImage(named: "Image4")!, repName: "계약4", startDate: formatter.date(from: "2023.9.01")!, endDate: formatter.date(from: "2024.04.30")!, status: "완료"),
]
