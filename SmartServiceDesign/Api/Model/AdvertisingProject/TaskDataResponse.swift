//
//  TaskDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/17/24.
//

import Foundation

import Foundation

struct TaskDataResponse: Codable {
    let result: Bool
    let data: [TaskData]
    let pagination: PaginationData_Task
    
    enum CodingKeys: String, CodingKey {
        case result, data, pagination
    }
}

struct TaskData: Codable {
    let taskIdx: Int
    let scheduleIdx: Int
    let taskName: String
    let oneLineExplain: String
    let taskStartDt: String
    let taskEndDt: String
    let taskStatus: String
    let hideOrNot: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    let schedule: ScheduleData_Task

    enum CodingKeys: String, CodingKey {
        case taskIdx = "task_idx"
        case scheduleIdx = "schedule_idx"
        case taskName = "task_name"
        case oneLineExplain = "one_line_explain"
        case taskStartDt = "task_start_dt"
        case taskEndDt = "task_end_dt"
        case taskStatus = "task_status"
        case hideOrNot = "hide_or_not"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case schedule
    }
}

struct ScheduleData_Task: Codable {
    let scheduleIdx: Int
    let projectIdx: Int
    let scheduleName: String
    let oneLineExplain: String
    let scheduleStartDt: String
    let scheduleEndDt: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    let project: ProjectData_Task

    enum CodingKeys: String, CodingKey {
        case scheduleIdx = "schedule_idx"
        case projectIdx = "project_idx"
        case scheduleName = "schedule_name"
        case oneLineExplain = "one_line_explain"
        case scheduleStartDt = "schedule_start_dt"
        case scheduleEndDt = "schedule_end_dt"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case project
    }
}

struct ProjectData_Task: Codable {
    let projectIdx: Int
    let brandIdx: Int
    let userIdx: Int
    let projectName: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case projectIdx = "project_idx"
        case brandIdx = "brand_idx"
        case userIdx = "user_idx"
        case projectName = "project_name"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

struct PaginationData_Task: Codable {
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int

    enum CodingKeys: String, CodingKey {
        case total
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}
