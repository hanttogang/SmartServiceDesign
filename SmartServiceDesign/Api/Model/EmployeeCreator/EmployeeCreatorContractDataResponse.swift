//
//  EmployeeCreatorDataResponse.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/12/24.
//

import Foundation

import Foundation

struct EmployeeCreatorContractDataResponse: Codable {
    let result: Bool
    let data: [HumanResourceContract]
    let pagination: Pagination

    enum CodingKeys: String, CodingKey {
        case result
        case data
        case pagination
    }
}

struct HumanResourceContract: Codable {
    let humanResourceContractIdx: Int
    let humanResourceIdx: Int
    let contractName: String
    let oneLineExplain: String
    let contractStartDt: String
    let contractEndDt: String
    let contractStatus: String
    let contractAmount: Int
    let contractImage: String
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?
    let humanResource: HumanResource

    enum CodingKeys: String, CodingKey {
        case humanResourceContractIdx = "human_resource_contract_idx"
        case humanResourceIdx = "human_resource_idx"
        case contractName = "contract_name"
        case oneLineExplain = "one_line_explain"
        case contractStartDt = "contract_start_dt"
        case contractEndDt = "contract_end_dt"
        case contractStatus = "contract_status"
        case contractAmount = "contract_amount"
        case contractImage = "contract_image"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
        case humanResource = "human_resource"
    }
}

struct HumanResource: Codable {
    let humanResourceIdx: Int
    let userIdx: Int
    let masterUserIdx: Int
    let position: String
    let department: String
    let userName: String
    let userType: String?
    let userEmail: String
    let userImage: String
    let userPhone: String
    let oneLineExplain: String
    let manageType: String?
    let firstCreateDt: String
    let lastUpdateDt: String
    let deleteDt: String?

    enum CodingKeys: String, CodingKey {
        case humanResourceIdx = "human_resource_idx"
        case userIdx = "user_idx"
        case masterUserIdx = "master_user_idx"
        case position
        case department
        case userName = "user_name"
        case userType = "user_type"
        case userEmail = "user_email"
        case userImage = "user_image"
        case userPhone = "user_phone"
        case oneLineExplain = "one_line_explain"
        case manageType = "manage_type"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case deleteDt = "delete_dt"
    }
}

struct Pagination: Codable {
    let total: Int
    let currentPage: Int
    let totalPage: Int
    let block: Int
    let currentBlock: Int
    let totalBlock: Int

    enum CodingKeys: String, CodingKey {
        case total
        case currentPage = "current_page"
        case totalPage = "total_page"
        case block
        case currentBlock = "current_block"
        case totalBlock = "total_block"
    }
}
